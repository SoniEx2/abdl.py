A Boneless Datastructure Language
=================================

Installing
----------

1. Pick a repo: [web+ganarchy:0f74bd87a23b515b45da7e6f5d9cc82380443dab](web+ganarchy:0f74bd87a23b515b45da7e6f5d9cc82380443dab).
2. `pip install -e <repo>#egg=gan0f74bd87a23b515b45da7e6f5d9cc82380443dab`
3. `import abdl`
4. Use `gan0f74bd87a23b515b45da7e6f5d9cc82380443dab` as the dependency in `setup.py` or equivalent.

Usage
-----

TODO

Language Reference
------------------

See module level documentation:

```
>>> import abdl
>>> help(abdl)
```

License
-------

ABDL is AGPL-3.0-or-later. See LICENSE.txt
